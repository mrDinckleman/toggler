/*!
 * toggler - jQuery Plugin
 * version: 1.0.1 (Fri, 8 Nov 2013)
 * @written for jQuery v1.10.1
 *
 * Examples https://bitbucket.org/mrDinckleman/toggler
 *
 * Copyright 2013 Sergey Kovalchuk - admin@vitam.in.ua
 */

(function ($) {
    "use strict";

    // http://msdn.microsoft.com/en-us/library/ms537509%28v=vs.85%29.aspx
    //
    // Returns the version of Internet Explorer or a -1
    // (indicating the use of another browser).
    function getInternetExplorerVersion () {
        var rv = -1; // Return value assumes failure.
        if (navigator.appName === 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
            if (re.exec(ua) !== null) {
                rv = parseFloat(RegExp.$1);
            }
        }
        return rv;
    }

    // Global variables
    var IE7 = (function () { // Detect IE7 and less
            var ver  = getInternetExplorerVersion();
            return (ver > -1 && ver < 8.0);
        }()),
        transitions = Modernizr.csstransitions || false; // CSS Transitions support

    var TogglerObj = function (element, options) {
        // Defaults are below
        var settings = $.extend({}, $.fn.toggler.defaults, options);

        // Useful variables
        var animate = (settings.animSpeed !== 0) && !IE7; // флаг включения анимации

        // Get this toggler
        var toggler = $(element); // элемент тогглера

        // Закрытие блока
        var close = function () {
            var block = this;
            settings.beforeClose.call(block);
            if (animate) {
                block.addClass('closing').children('.accordion-body').slideUp(settings.animSpeed, function () { // скрыть блок с анимацией
                    block.addClass('collapsed').removeClass('closing');
                    settings.afterClose.call(block);
                });
            } else {
                block.addClass('collapsed').children('.accordion-body').hide(); // скрыть блок без анимации
                settings.afterClose.call(block);
            }
        };
        // Открытие блока
        var open = function () {
            var block = this;
            settings.beforeOpen.call(this);
            if (animate) {
                block.addClass('opening').children('.accordion-body').slideDown(settings.animSpeed, function () { // показать блок с анимацией
                    block.removeClass('collapsed opening');
                    settings.afterOpen.call(block);
                });
            } else {
                block.removeClass('collapsed').children('.accordion-body').show(); // показать блок без анимации
                settings.afterOpen.call(block);
            }
            if (settings.onlyOne) { // если к показу допускается только один блок
                block.siblings('.accordion-group:not(".collapsed"), .accordion-group.opening').each(function () { // проход по остальным открытым и открывающимся блокам
                    $(this).children('.accordion-body').stop(); // остановить анимацию других блоков
                    close.call($(this)); // скрыть остальные блоки
                });
            }
        };
        // Обработка кликов по кнопкам
        var bindEvents = function () {
            toggler.on('click', '.accordion-open, .accordion-close, .accordion-toggle', function (e) { // при клике по кнопкам
                e.preventDefault();
                var block = $(this).closest('.accordion-group'); // блок, в котором кликнули по кнопке
                if (($(this).is('.accordion-open') || $(this).is('.accordion-toggle')) && block.hasClass('collapsed')) { // если блок скрыт и кликнули по OPEN или TOGGLE
                    open.call(block); // открыть блок
                } else if (($(this).is('.accordion-close') || $(this).is('.accordion-toggle')) && !block.hasClass('collapsed')) { // если блок открыт и кликнули по CLOSE или TOGGLE
                    // если стоит ограничение на последний открытый блок и этот блок - последний открытый, то не закрывать его, иначе - закрыть
                    if (!(settings.atLastOne && toggler.find('.accordion-group:not(".collapsed")').size() === 1)) {
                        close.call(block);
                    }
                }
            });
        };
        // Старт плагина
        var start = function () {
            toggler.children('.accordion-group').each(function () { // проход по всем блокам в тогглере
                var block = $(this);
                block.toggleClass('collapsed', !settings.showOnInit.call(block)); // скрыть блок, если settings.showOnInit() для него вернет false
            });
            bindEvents(); // подключить события на кнопки
        };

        start(); // старт плагина
        return this;
    };

    $.fn.toggler = function (options) {
        return this.each(function () {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('toggler')) { // если слайдер уже инициализирован
                return element.data('toggler'); // выйти
            }
            // Pass options to plugin constructor
            var Toggler = new TogglerObj(this, options); // вызвать плагин
            // Store plugin object in this element's data
            element.data('toggler', Toggler); // сохранить запись об инициализации
        });
    };

    // Default settings
    $.fn.toggler.defaults = {
        // Время анимации закрытия/открытия (int)
        // при 0 - анимация отсутствует
        // для IE <= 7 значение всегда 0
        animSpeed: 300,

        // Показ последнего блока (bool)
        // true - открытым остается как минимум один блок
        // false - можно закрыть все блоки
        atLastOne: false,

        // Одновременный показ нескольких блоков (bool)
        // true - одновременно показывать только один блок
        // false - могут быть открыты одновременно несколько блоков
        onlyOne: true,

        // Показ блока при старте (применяется для каждого блока)
        // return true - блок будет показан
        // return false - блок будет скрыт
        showOnInit: function () {
            return false;
        },

        // Hooks
        beforeOpen:  $.noop(),
        afterOpen:   $.noop(),
        beforeClose: $.noop(),
        afterClose:  $.noop()
    };
}(jQuery));